﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Core.Constants;
using OnlineStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineStore.Infrastructure.Data
{
    public class OnlineStoreDbContextSeed
    {
        public static async Task SeedAsync(OnlineStoreDbContext onlineStoreDbContext, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            await roleManager.CreateAsync(new Role { 
                Id = Guid.NewGuid(),
                Name = OnlineStoreConstants.Roles.GUESTS
            });
            await roleManager.CreateAsync(new Role
            {
                Id = Guid.NewGuid(),
                Name = OnlineStoreConstants.Roles.REGISTERED
            });
            await roleManager.CreateAsync(new Role
            {
                Id = Guid.NewGuid(),
                Name = OnlineStoreConstants.Roles.ADMINISTRATORS
            });

            string adminUserName = "admin@onlinestore.com";
            var adminUser = new User { UserName = adminUserName, Email = adminUserName };
            await userManager.CreateAsync(adminUser, "Qwerty1!");
            adminUser = await userManager.FindByNameAsync(adminUserName);
            await userManager.AddToRoleAsync(adminUser, OnlineStoreConstants.Roles.ADMINISTRATORS);

            if (!await onlineStoreDbContext.Products.AnyAsync())
            {
                await onlineStoreDbContext.Products.AddRangeAsync(GetProducts());
                await onlineStoreDbContext.SaveChangesAsync();
            }
        }

        static IEnumerable<Product> GetProducts()
        {
            return new List<Product>()
            {
                new Product("7.6' Смартфон Samsung Galaxy Z Fold2 256 ГБ коричневый",
                    "4703187",
                    "Смартфон Samsung Galaxy Z Fold2 коричневого цвета имеет гибкий дисплей, что позволяет превратить компактную модель в полноценный планшет диагональю 7.6 дюйма. Устройство оптимально для игр, работы, общения и развлечений. Высокоскоростной процессор обеспечивает работу и загрузку программ без сбоев. Сенсор моментально реагирует на касание, а для удобства предусмотрена беспроводная зарядка.",
                    159999,
                    10),
                new Product("6.7' Смартфон Apple iPhone 12 Pro Max 512 ГБ",
                    "4722580",
                    "Это iPhone 12 Pro Max. Увеличенный дисплей Super Retina XDR 6,7 дюйма. Передняя панель Ceramic Shield, с которой риск повреждений дисплея при падении в 4 раза ниже. ",
                    128799,
                    3),
                new Product("6.7' Смартфон Apple iPhone 12 Pro Max 512 ГБ",
                    "4722580",
                    "Это iPhone 12 Pro Max. Увеличенный дисплей Super Retina XDR 6,7 дюйма. Передняя панель Ceramic Shield, с которой риск повреждений дисплея при падении в 4 раза ниже. ",
                    128799,
                    3),
            };
        }
    }
}
