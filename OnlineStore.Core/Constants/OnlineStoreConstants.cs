﻿namespace OnlineStore.Core.Constants
{
    public static class OnlineStoreConstants
    {
        public static class Authorization
        {
            public const string JWT_SECRET_KEY = "56aeed88-468e-4daf-ba90-676b2f773678";
        }

        public static class Roles
        {
            public const string GUESTS = "Guests";
            public const string REGISTERED = "Registered";            
            public const string ADMINISTRATORS = "Administrators";            
        }
    }
}
