﻿using Ardalis.Specification;
using OnlineStore.Core.Entities.BasketAggregate;
using System;
using System.Linq;

namespace OnlineStore.Core.Specifications
{
    public sealed class BasketWithItemsSpecification : Specification<Basket>
    {
        public BasketWithItemsSpecification(Guid? basketId = null, Guid? userId = null)
        {
            Query.Where(b => (!basketId.HasValue || b.Id == basketId)
                          && (!userId.HasValue || b.UserId == userId)
                ).Include(b => b.Items);
        }
    }
}
