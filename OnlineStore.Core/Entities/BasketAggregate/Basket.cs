﻿using OnlineStore.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OnlineStore.Core.Entities.BasketAggregate
{
    public class Basket : BaseEntity, IAggregateRoot
    {
        public Guid UserId { get; private set; }
        private readonly List<BasketItem> _items = new List<BasketItem>();
        public IReadOnlyCollection<BasketItem> Items => _items.AsReadOnly();

        public Basket(Guid userId)
        {
            UserId = userId;
        }

        public void AddItem(Guid productId, decimal unitPrice, int quantity = 1)
        {
            if (!Items.Any(i => i.ProductId == productId))
            {
                _items.Add(new BasketItem(productId, quantity, unitPrice));
                return;
            }

            var existingItem = Items.FirstOrDefault(i => i.ProductId == productId);
            existingItem.AddQuantity(quantity);
        }

        public void RemoveEmptyItems()
        {
            _items.RemoveAll(i => i.Quantity == 0);
        }
    }
}
