﻿using MediatR;
using System;

namespace OnlineStore.Core.Entities
{
    public abstract class BaseDomainEvent : INotification
    {
        public DateTime DateOccurred { get; protected set; } = DateTime.UtcNow;
    }
}
