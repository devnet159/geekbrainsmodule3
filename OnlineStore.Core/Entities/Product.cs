﻿using Ardalis.GuardClauses;
using OnlineStore.Core.Interfaces;

namespace OnlineStore.Core.Entities
{
    public class Product : BaseEntity, IAggregateRoot
    {
        public Product(string name, string sku, string description, decimal price, int stockQuantity)
        {
            Name = Guard.Against.NullOrEmpty(name, nameof(name));
            Sku = Guard.Against.NullOrEmpty(sku, nameof(sku));
            Price = Guard.Against.Negative(price, nameof(price));
            StockQuantity = Guard.Against.Negative(stockQuantity, nameof(stockQuantity));

            Description = description;
        }

        public string Name { get; private set; }

        public string Sku { get; private set; }

        public string Description { get; private set; }

        public decimal Price { get; private set; }

        public int StockQuantity { get; private set; }
    }
}
