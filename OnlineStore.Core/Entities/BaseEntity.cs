﻿using System;
using System.Collections.Generic;

namespace OnlineStore.Core.Entities
{
    public abstract class BaseEntity 
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime? LastModifiedOnUtc { get; set; }

        public List<BaseDomainEvent> Events = new();

        protected BaseEntity()
        {
            Id = Guid.NewGuid();

            var utcNow = DateTime.UtcNow;
            CreatedOnUtc = utcNow;
            LastModifiedOnUtc = utcNow;
        }
    }
}
