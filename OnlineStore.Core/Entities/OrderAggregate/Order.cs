﻿using Ardalis.GuardClauses;
using OnlineStore.Core.Interfaces;
using System;
using System.Collections.Generic;

namespace OnlineStore.Core.Entities
{
    public class Order : BaseEntity, IAggregateRoot
    {
        private Order() {}

        public Order(Guid userId, List<OrderItem> items)
        {
            UserId = Guard.Against.NullOrEmpty(userId, nameof(userId));
            _orderItems = Guard.Against.Null(items, nameof(items));
        }

        public Guid UserId { get; set; }

        private readonly List<OrderItem> _orderItems = new();
        public IReadOnlyCollection<OrderItem> OrderItems => _orderItems.AsReadOnly();

        public decimal Total()
        {
            var total = 0m;
            foreach (var item in _orderItems)
            {
                total += item.Price * item.Quantity;
            }

            return total;
        }

    }
}
