﻿using System;

namespace OnlineStore.Core.Interfaces
{
    public interface IEntity
    {
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOnUtc { get; set; }
    }
}
