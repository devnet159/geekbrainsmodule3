﻿using System.Threading.Tasks;

namespace OnlineStore.Core.Interfaces
{
    public interface ITokenClaimsService
    {
        Task<string> GetTokenAsync(string userName);
    }
}
