﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineStore.Core.Interfaces
{
    public interface IBasketService
    {
        Task AddItemToBasketAsync(Guid basketId, Guid productId, decimal price, int quantity = 1);
        Task SetQuantitiesAsync(Guid basketId, Dictionary<string, int> quantities);
        Task DeleteBasketAsync(Guid basketId);
        Task TransferBasketAsync(Guid? anonymousId, Guid? userId);
    }
}
