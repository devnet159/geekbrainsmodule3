﻿using Ardalis.GuardClauses;
using OnlineStore.Core.Entities.BasketAggregate;
using OnlineStore.Core.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineStore.Core.Extensions
{
    public static class GuardExtensions    
    {
        public static void NullBasket(this IGuardClause guardClause, Guid basketId, Basket basket)
        {
            if (basket == null)
                throw new BasketNotFoundException(basketId);
        }

        public static void EmptyBasketOnCheckout(this IGuardClause guardClause, IReadOnlyCollection<BasketItem> basketItems)
        {
            if (!basketItems.Any())
                throw new EmptyBasketOnCheckoutException();
        }
    }
}
